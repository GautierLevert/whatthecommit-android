package org.mybop.whatthecommit

import com.github.tomakehurst.wiremock.client.WireMock.*
import com.github.tomakehurst.wiremock.junit.WireMockRule
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory

/**
 * Unit tests for [org.mybop.whatthecommit.CommitmentClient]
 */
class CommitmentClientTest {

    private val commitmentClient = Retrofit.Builder()
            .baseUrl("http://localhost:8089/")
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(ScalarsConverterFactory.create())
            .build()
            .create(CommitmentClient::class.java)

    @get:Rule
    val wireMock = WireMockRule(8089)

    @Before
    fun setUp() {
        stubFor(get(urlMatching("/.*"))
                .willReturn(
                        notFound()
                                .withHeader("Content-Type", "text/html; charset=UTF-8")
                                .withBody("<html><title>404: Not Found</title><body>404: Not Found</body></html>")
                )
        )

        stubFor(get(urlEqualTo("/413be72716ef7ac83fee77116d8e61cd/index.txt"))
                .willReturn(
                        ok("I don't know why. Just move on.\n")
                )
        )

        stubFor(get(urlEqualTo("/index.txt"))
                .willReturn(
                        ok("copy and paste is not a design pattern\n")
                )
        )
    }

    /**
     * Test if [random message][org.mybop.whatthecommit.CommitmentClient.randomMessage] method works correctly
     */
    @Test
    fun randomMessage() {
        val request = commitmentClient.randomMessage()

        assertThat(request.blockingGet()).isEqualTo("copy and paste is not a design pattern\n")
    }

    /**
     * Test if method to [find message by hash][org.mybop.whatthecommit.CommitmentClient.findMessage] works correctly
     */
    @Test
    fun findMessage() {
        val request = commitmentClient.findMessage("413be72716ef7ac83fee77116d8e61cd")

        assertThat(request.blockingGet()).isEqualTo("I don't know why. Just move on.\n")
    }

    /**
     * Test if not found return code is managed correctly
     */
    @Test
    fun badHash() {
        val request = commitmentClient.findMessage("bad_hash")

        request.subscribe { message, throwable ->
            assertThat(message).isNull()
            assertThat(throwable).isInstanceOf(HttpException::class.java)
            assertThat((throwable as HttpException).code()).isEqualTo(404)
        }
    }
}
