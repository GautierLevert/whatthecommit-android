package org.mybop.whatthecommit

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import toothpick.Toothpick
import javax.inject.Inject

class CommitViewModel(application: Application) : AndroidViewModel(application) {

    @Inject
    internal lateinit var commitmentClient: CommitmentClient

    private val cachedDownload = BehaviorSubject.create<String>()

    private val disposable = CompositeDisposable()

    init {
        val scope = Toothpick.openScopes(application, this)
        Toothpick.inject(this, scope)
    }

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
        Toothpick.closeScope(this)
    }

    val commit: Observable<String>
        get() = cachedDownload
                .observeOn(AndroidSchedulers.mainThread())
                .hide()

    fun random() {
        disposable.add(
                commitmentClient.randomMessage()
                        .subscribeOn(Schedulers.io())
                        .subscribe({
                            cachedDownload.onNext(it)
                        }, {
                            cachedDownload.onError(it)
                        })
        )
    }

    fun find(hash: String) {
        disposable.add(
                commitmentClient.findMessage(hash)
                        .subscribeOn(Schedulers.io())
                        .subscribe({
                            cachedDownload.onNext(it)
                        }, {
                            cachedDownload.onError(it)
                        })
        )
    }
}