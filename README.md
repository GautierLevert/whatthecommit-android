# About

This project is an Android application for the web site

    http://whatthecommit.com/

> The website belongs to Nick Gerakines <nick@gerakines.net>
> Source code available on GitHub [ngerakines/commitment](https://github.com/ngerakines/commitment)

# License

This project and its contents are open source under the GPL license.
